
fetch('/api/form-data')
.then(function(res) {
  return res.json()
}) 
.then(function(res) {
  $('.ui.form').form({
    on: 'blur',
    fields: {
      email:          ['empty','email'],
      privacy_notice: 'checked',
      subject:        ['empty', 'minLength[5]'],
      message:        ['empty', 'minLength[100]'],
      category:       ['empty'],
      captcha:        ['empty', 'is['+res.captcha.text+']']
    },
    onSuccess: promptAndSubmit
  });
  $('#captcha')[0].innerHTML = res.captcha.data
  return res.categories.map(function(item,i) {
    const { icon, text, code } = item
    return {
      name: text,
      value: code,
      selected: text == '',
      icon
    }
  })
})
.then(function(values) {
  $('.ui.dropdown').dropdown({
    values,
  });
});

$('#message').on('keyup', function(event) {
  const length = event.target.value.length
  $('.ui.text')[0].innerHTML = length + ' / 4000'
})


function promptAndSubmit(event, fields) {
  console.log(fields)
  $('.ui.button.submit').addClass('loading');
  fetch('/api/submit', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(fields),
  })
  .then(response => response.json())
  .then(data => {
    console.log(data);
    if (data.success) {
      $('body').toast({
        message: `Vielen Dank für Ihre Nachricht!`,
        displayTime: 6000,
        class: 'inverted yellow',
      })
      $('.ui.form').form('clear');
      $('.ui.button.submit').removeClass('loading');
    }
  })
  .catch((error) => {
    $('body').toast({
      message: `Ihre Nachricht konnte nicht versendet werden! Bitte probieren Sie es erneut oder benachrichtigen den Administrator.`,
      displayTime: 6000,
      class: 'inverted red',
    })
    $('.ui.button.submit').removeClass('loading');
    $('.ui.button.submit').addClass('error');
    setTimeout(function() {
      $('.ui.button.submit').removeClass('error');
    }, 3000)
    console.error(error);
  });
;
}

$('.ui.button.submit').click(function() {
  $('.ui.form').form('submit')
})