const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
console.log(process.env.SENDGRID_API_KEY)

const sendOptIn = ({email, category, subject, message, ip, user_agent}) => {
  const msg = {
    to: 'demokratischerwiderstand@protonmail.com',
    from: email,
    replyTo: email,
    subject: '['+category+'] '+subject,
    text: message + '\n\nIP: '+ip+'\nUser-Agent: '+user_agent
  };
  return sgMail
  .send(msg)
}

module.exports = {
  sendOptIn
}