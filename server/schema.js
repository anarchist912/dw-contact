const message = {
  "id": "order",
  "type": "object",
  "properties": {
    email: {type: "string"},
    message: {type: "string"},
    subject: {type: "string"},
    category: {type: "string"}
  },
  "required": ["email", "message", "subject", "category"]
}

module.exports = {
  message
}